Migração da Agencia de Notícias Ciência e Cultura
=================================================

Este é um script para migrar o conteúdo do WordPress para o Noosfero via Web Scraping.
Não é generalista mas pode ser adaptado para migrar outros sites sendo WP ou não.

Passos para migrar:
1. Criar uma comunidade com o identificador `cienciaecultura` no Noosfero.
2. Criar dois blogs com os slugs: `noticias` e `pesquisadores`.
3. Instalar esse script no servidor: `git clone https://gitlab.com/colivre/migracao-cienciaecultura.git`.
4. Mudar para o usuário do noosfero: `su -c bash noosfero`.
5. Entre no diretório do Noosfero: `cd /usr/share/noosfero`.
6. Inicie a migração: `/opt/migracao-cienciaecultura/migrate.sh`.
(ele rodará com `RAILS_ENV=production`)

Esse script baixa os artigos do site cienciaecultura.ufba.br e os registra no
Noosfero local. É preciso rodar esse script com o usuário do Noosfero e no
seu diretório de instalação.

Caso esse script pare no meio da migração, basta reiniciar o processo. Ele
registrará os artigos já migrados em um log.

Ao reiniciar ele vasculhará página por página do servidor do cienciaecultura,
mas só criará novo artigo no Noosfero caso não o tenha anotado no log acima.

Como muitas imagens serão migradas com os artigos, pode ser interessante definir
um Kind para essa comunidade com quota "unlimited".
