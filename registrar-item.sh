#!/bin/bash -e
. "$(dirname "$0")/globals.sh"

work_dir=$(pwd)
category=$(echo $1 | sed 's/s$//')
item=$2

n='
'
tr_f='[A-ZÁÀÃÂÉÊÍÓÕÔÚÇ]'
tr_t='[a-záàãâéêíóõôúç]'

test $category = 'opiniao' && category='opinião'

case $category in
  pesquisador)
    splitRE='.*<a class="titulo-item" href="([^"]+)">(.*?)</a>.*'
    url=$(  echo $item | sed -r "s#$splitRE#\1#")
    title=$(echo $item | sed -r "s#$splitRE#\2#" | sed -r "s#<[^>]+>##g; s#^\s|\s\$##g; s#'#’#g")
    tema=pesquisador
    ;;
  evento)
    splitRE='.*<span class="tema-item">(\s*[0-9].*?)</span>.*<a class="titulo-item" href="([^"]+)">(.*?)</a>.*'
    if (echo $item | egrep -q "$splitRE"); then
      url=$(  echo $item | sed -r "s#$splitRE#\2#")
      ev_date=$(echo $item|sed -r "s#$splitRE#\1#" | sed -r "s#<[^>]+>##g; s#^\s|\s\$##g; s#'#’#g; s# e outubro# de outubro#")
      title=$(echo $item | sed -r "s#$splitRE#\3#" | sed -r "s#<[^>]+>##g; s#^\s|\s\$##g; s#'#’#g")
    else
      splitRE='.*<a class="titulo-item" href="([^"]+)">(.*?)</a>.*'
      url=$(  echo $item | sed -r "s#$splitRE#\1#")
      title=$(echo $item | sed -r "s#$splitRE#\2#" | sed -r "s#<[^>]+>##g; s#^\s|\s\$##g; s#'#’#g")
      ev_date='1 de janeiro de 1999'
    fi
    tema=evento
    ;;
  *)
    temaRE='.*<span class="tema-item">([^<]*)</span>.*'
    splitRE='.*<a class="titulo-item" href="([^"]+)">(.*?)</a>.*'
    url=$(  echo $item | sed -r "s#$splitRE#\1#")
    title=$(echo $item | sed -r "s#$splitRE#\2#" | sed -r "s#<[^>]+>##g; s#^\s|\s\$##g; s#'#’#g")
    if (echo $item | egrep -q "$temaRE"); then
      tema=$( echo $item | sed -r "s#$temaRE#\1#" | sed -r "s#<[^>]+>##g; s#^\s|\s\$##g; s#'#’#g" | tr "$tr_f" "$tr_t")
    else
      tema=''
    fi
    ;;
esac

baseURL='http://www.cienciaecultura.ufba.br/agenciadenoticias'
if [ \
  "$url" = "$baseURL/noticias/ultimas/18606/" -o \
  "$url" = "$baseURL/pesquisadores/henriette-ferreira-gomes/" -o \
  "$url" = "$baseURL/pesquisadores/katia-maria-coelho-de-carvalho-custodio-katia-de-carvalho/" \
   ]; then
  echo '>> Link quebrado.'
  exit 0
fi

if grep -q "^$url$" $success_log; then
  echo ">> $url já foi migrada. :-)"
  exit 0
else
  echo ">> Nova!"
fi

if [ $category = noticia ]; then
  base='^https?://www.cienciaecultura.ufba.br/agenciadenoticias'
  if (!( echo $url | egrep -q "$base/noticias/.*" )) ||
     ( echo $url | egrep -q "$base/noticias/ultimas/.*" ); then
    echo ">> $url não é uma notícia."
    echo ">> Será migrada na interação adequada."
    exit 0
  fi
fi

sleep $((1+$RANDOM%3)) # Não abuse do servidor.

slug=$( echo $url | sed -r 's#^.*/([^/]+)/([^/]+)/?$#\1--\2#; s#[^-a-z0-9]+#x#gi' )
imgs_json="$(realpath "$work_dir/$slug.images.json")"

test $category = entrevista && title="Entrevista: $title"

if (echo -n $tema | grep -q ' '); then
  tema="$tema$n$(
    echo -n $tema | sed -r 's/(^| )(n[oa]s?|e|a|d[eao]|com|pa?ra)( |$)/ /ig; s/^\s|\s$//g; s/\s+/\n/g'
  )"
fi

download $url $slug.ORIG.html

if (grep -q 'id="data"' $slug.ORIG.html); then
  if (egrep -q '<span id="data">Atualizado em(\s*|\s+[^0-9].*)</span>' $slug.ORIG.html); then
    pub_day=01
    pub_month=01
    pub_year=1999
    pub_time=12:00
  else
    eval $(grep 'id="data"' $slug.ORIG.html |
    sed -r 's#.*<span id="data">Atualizado em ([0-9]+)( [ae] [0-9]+|[0-9,e ]*)? DE ([a-z]+)( DE ([0-9]+))?( ás ([0-9:]+))?.*#\
    pub_day=\1;\
    pub_month=\3;\
    pub_year=\5;\
    pub_time=\7:00;\
    #i')
    test ${#pub_day} -lt 2 && pub_day="0$pub_day"
    test $pub_time = ':00' && pub_time=12:00
    test -z "$pub_year" && pub_year=1999
    m_num=0
    for m in jan fev mar abr mai jun jul ago set out nov dez; do
      m_num=$(($m_num+1))
      pub_month=$(echo $pub_month | sed "s/$m[a-z]*/$m_num/")
    done
    test ${#pub_month} -lt 2 && pub_month="0$pub_month"
  fi
  pub="$pub_year-$pub_month-${pub_day}T$pub_time+300"
else
  pub="$(date +%FT%T+300)"
fi

if [ $category = evento ]; then
  eval $(echo $ev_date | sed -r 's#\s*([0-9]+)( [ae] [0-9]+|[0-9,e ]*)? de ([a-z]+)( de ([0-9]+))?.*#\
  ev_day=\1;\
  ev_month=\3;\
  ev_year=\5;\
  #i')
  test ${#ev_day} -lt 2 && ev_day="0$ev_day"
  test -z "$ev_year" && ev_year=1999
  m_num=0
  for m in jan fev mar abr mai jun jul ago set out nov dez; do
    m_num=$(($m_num+1))
    ev_month=$(echo $ev_month | sed "s/$m[a-z]*/$m_num/")
  done
  test ${#ev_month} -lt 2 && ev_month="0$ev_month"

  ev_date="$ev_year-$ev_month-${ev_day}T00:00+300"
fi

abstract=$(
  echo $(cat $slug.ORIG.html) |
  sed -r "s#.*<meta name='description' content='([^']*)'.*#\1#i"
)

echo '' > $slug.html

if [ $category = pesquisador ]; then
  (
    echo '<ul>'
    echo $(cat $slug.ORIG.html) |
    sed -r 's#.* id="titulo-noticia"(.*) class="conteudo-noticia".*#\1#' |
    sed -r 's#<h2[^>]*>#\n<li>#g; s#</h2>#</li>\n>#g' |
    grep '^<li'
    echo '</ul>'
  ) > $slug.html
fi

echo $(cat $slug.ORIG.html) | sed -r '
  s#<div [^>]*class="conteudo-noticia"#\n<div class="conteudo-noticia"#ig;
  s#<div [^>]*id="comments">#\n<div id="comments">#ig
' |
grep '<div class="conteudo-noticia"' |
sed -r '
  s#<div class="conteudo-noticia"[^>]*>|</div>\s*$"##g;
  s#<div [^>]*class="midia-tweet"[^>]*>.*<iframe src="https?://www.facebook.com/plugins/like.php[^>]*></iframe>\s*</div>##g;
  s#<a [^>]*href="https?://www.cienciaecultura.ufba.br/[^>]*\.(jpe?g|png|gif)"[^>]*>##ig;
  s#</script>#</script>\n#ig; s#<script.*</script>##ig;
  s#\r# #g
' >> $slug.html

echo -n '' > $imgs_json
(
  if [ $category = pesquisador ]; then
    echo $(cat $slug.ORIG.html) |
    sed -r '
      s#<div[^>]* id="titulo-noticia"#\n<div id="titulo-noticia">#;
      s#</div>#\n#g;
    ' |
    grep '<div id="titulo-noticia">' |
    sed -r 's#.*<img[^>]* src="(https?:[^"]*)".*#\1#'
  fi

  sed -r 's#<img [^>]*src="([^"]+)"[^>]*>#\n<img src="\1">\n#ig' $slug.html |
  egrep '<img src="https?://www.cienciaecultura.ufba.br/([^"]+)">' |
  sed -r 's#<img src="([^"]+)">#\1#ig'
) | uniq |
grep -v 'prof-dr-angelo-serpa.jpg' | # ignore broken pic
while read img_url; do
  img_file=$( echo $img_url | sed 's#.*wp-content/uploads/##; s#/#--#g; s#[^-.a-z0-9]#x#gi' )
  mime="image/$( echo $img_file | tr [A-Z] [a-z] | sed -r 's#.*\.##; s#jpg#jpeg#' )"
  if wget $img_url -O $img_file >/dev/null; then
    sed -ri "s#$img_url#/cienciaecultura/noticias/$slug/$img_file#g" $slug.html
    echo "{ \"file\": \"$img_file\", \"mime\": \"$mime\" }" >> $imgs_json
  else
    echo ">> Hummm... parece que temos um link quebrado para $img_url" >&2
  fi
done

#echo '=== INI ================================================================='
#cat $slug.html
#echo '=== IMGS ================================================================'
#cat $imgs_json
#echo '=== FIM ================================================================='

echo -n ">> Importando:
-> name: $title
-> slug: $slug
-> pub:  $pub
-> tags: '$category'$(echo "$tema" | while read t; do echo -n ", '$t'"; done)
"

cd "$NOOSFERO_DIR"
rails runner "
profile = Profile['cienciaecultura']
if '$category' == 'pesquisador'
  blog = profile.blogs.find_by_slug 'pesquisadores'
else
  blog = profile.blogs.find_by_slug 'noticias'
end
if article = blog.posts.find_by_slug('$slug')
  puts '>> Remove previews $slug'
  article.destroy!
end
if '$category' == 'evento'
  article = Event.create!(
    name: '$title',
    slug: '$slug',
    abstract: '<p>$abstract</p>',
    published_at: '$pub',
    start_date: '$ev_date',
    body: File.read('$work_dir/$slug.html'),
    profile: profile,
    parent: blog
  )
else
  article = TextArticle.create!(
    name: '$title',
    slug: '$slug',
    abstract: '<p>$abstract</p>',
    published_at: '$pub',
    body: File.read('$work_dir/$slug.html'),
    tag_list: ['$category'$(echo "$tema" | while read t; do echo -n ", '$t'"; done)],
    profile: profile,
    parent: blog
  )
end
IO.readlines('$imgs_json').each do |img_json|
  img = JSON.parse(img_json)
  puts '-> Import img: ' + img['file']
  if '$category' == 'pesquisador'
    article.image = Image.create!(
      uploaded_data: Rack::Test::UploadedFile.new('$work_dir/'+img['file'], 'image/jpeg'),
    )
    article.save!
  else
    pic = UploadedFile.create!(
      uploaded_data: Rack::Test::UploadedFile.new('$work_dir/'+img['file'], 'image/jpeg'),
      profile: profile,
      parent: article
    )
    pic.slug = img['file']
    pic.save!
  end
end
IO.write('$success_log', \"$url\\n\", mode: 'a')
"
echo ">> ☺ $category $slug Gravado!"
