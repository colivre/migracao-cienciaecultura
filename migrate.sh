#!/bin/bash -e

export NOOSFERO_DIR="$(pwd)"
. "$(dirname "$0")/globals.sh"

echo -e "
Esse script baixa os artigos do site cienciaecultura.ufba.br e os registra no
Noosfero local. \033[1mÉ preciso rodar esse script com o usuário do Noosfero e no
seu diretório de instalação.\033[0m

Caso esse script pare no meio da migração, basta reiniciar o processo. Ele
registrará os artigos já migrados neste log:
$success_log

Ao reiniciar ele vasculhará página por página do servidor do cienciaecultura,
mas só criará novo artigo no Noosfero caso não o tenha anotado no log acima.
"

if ! (rake -T && rails runner 'Environment') >/dev/null; then
  echo "
  Ups... Parece que vc executou esse script fora do diretório do Noosfero, ou
  com o usuário errado.
  " >&2
  exit 1
fi

temp=$(mktemp -d /tmp/migrate-cienciaecultura.ufba.br-XXXXX)
cd $temp

baseURL=http://www.cienciaecultura.ufba.br/agenciadenoticias/arquivo

for categ in noticias entrevistas eventos; do
  curPag=1
  while download $baseURL/$categ/page/$curPag/ pag.html; do
    echo ">> Pag $curPag"
    echo $(cat pag.html) |
    sed -r 's#(</?div)#\n\1#g' |
    grep 'class="item-destaque"' |
    xargs --delimiter='\n' -L1 "$app_dir/registrar-item.sh" $categ
    curPag=$(($curPag+1))
  done
done

baseURL=http://www.cienciaecultura.ufba.br/agenciadenoticias/pesquisadores/?pagina
curPag=1
while (download $baseURL=$curPag/ pag.html && grep 'class="item-destaque"' pag.html); do
  echo ">> Pag $curPag"
  echo $(cat pag.html) |
  sed -r 's#(</?div)#\n\1#g' |
  grep 'class="item-destaque"' |
  xargs --delimiter='\n' -L1 "$app_dir/registrar-item.sh" pesquisador
  curPag=$(($curPag+1))
done

cd /tmp
rm -r $temp
echo 'Sucesso!'
