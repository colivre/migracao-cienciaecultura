#!/bin/bash -e

export app_dir="$(realpath "$(dirname "$0")")"
export RAILS_ENV=production

success_log="$NOOSFERO_DIR/log/migrate-cienciaecultura.ufba.br-success.log"
touch $success_log

function download() {
  url=$1
  file=$2
  if test -z "$url" -o -z "$file"; then
    echo "download function expects <url> and <file> params." >&2
    return 1
  fi
  echo ">> Downloading $url ..."
  header=$(mktemp)
  curl $url --dump-header $header > $file

  statusRE='^\s*HTTP/[0-9.]+ ([0-9]+)\s*([^\n\r]*).*'
  if ! egrep -q "$statusRE" $header; then
    echo "The download function can't undestand the received header." >&2
    echo "curl $url" >&2
    echo "=== INI HEADER ===========================================" >&2
    cat $header >&2
    echo "=== END HEADER ===========================================" >&2
    return 1
  fi

  eval "$( egrep "$statusRE" $header | sed -r "s#$statusRE#status=\1; statusMsg='\2'#" )"

  if [ $status -ge 300 -a $status -le 399 ]; then
    redirectTo=$( grep '^\s*Location:' $header | sed -r 's/^\s*Location:\s*([^\n\r]*).*/\1/' )
    echo ">> Redirect to $redirectTo ..."
    rm $header
    download $redirectTo $file
    return $?
  fi

  rm $header

  if [ $status -lt 200 -o $status -gt 299 ]; then
    echo "The server return \"Not OK\" HTTP Status \"$statusMsg\" ($status), for $url" >&2
    return 1
  fi

  return 0
}

